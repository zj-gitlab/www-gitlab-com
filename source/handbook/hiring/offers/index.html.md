---
layout: markdown_page
title: "Offer Packages and Contracts"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Offers

### Offer Package in Greenhouse

Once it is determined that a candidate will be moving to the offer stage, the hiring manager will communicate offer details to the recruiter, who is then responsible for submitting the official offer details through Greenhouse.

To create the offer package, move the candidate to the "Offer" stage in Greenhouse and select "Manage Offer." Input all required and relevant information, ensuring its correctness, and submit; then click `Request Approval`. **Please note that any changes in compensation packages will result in needing re-approval from each approver.**

Note that the offer package should include the candidate's proposed compensation in the most appropriate currency and format for their country of residence and job role. Annual and monthly salaries should be rounded up or down to the nearest whole currency unit and should always end with a zero (e.g., "50,110.00" or "23,500.00"). Hourly rates should be rounded to the nearest quarter-currency unit (e.g., 11.25/hr.).

For internal hires, be sure to include in the "Approval Notes" section the candidate's current level and position, as well as their compensation package.

You can also include any mitigating circumstances or other important details in the "Approval Notes" section of the offer details. If the comp has a variable component, please list base, on target earnings (OTE), and split in the "Approval Notes."

Please make sure that the level and position match the role page.

In case it is a public sector job family, please note (the lack of) clearances.

Information in the offer package for counter offers should include the following in the "Approval Notes" section:

   - New offer:
   - Original offer:
   - Candidate's salary expectation beginning of process:
   - Candidate's counter offer:

Anyone making comments regarding an offer should make sure to @mention the recruiter and hiring manager.

1. The People Ops Analyst will receive an email notifying them of the offer.
   * The People Ops Analyst will ensure the compensation is in line with our compensation benchmarks.
   * Only one approval is needed in order to move forward.
   * If the hire is not in a low location factor area above 0.9, the e-group member responsible for the function and the CFO will be notified.
1. Next, The People Business Partners will receive a notification to approve.
1. Next, the executive of the division will then receive a notification to approve.
1. Lastly, the CEO and Chief Culture Officer will receive a notification to approve
   * Only one approval is required in order to move forward with the offer.
   * Typically, the Chief Culture Officer will provide the final approval, but if the CCO is out of office, the CEO will be the final approver.


It is recommended to also ping approvers, especially the executive (and CEO if needed) in Slack with the message "Hiring approval needed for [Candidate Name] for [Position]" with a link to the candidate profile. To create the link, search for the candidate in Greenhouse, select the candidate, go to their offer details page, and copy the link. **Do not copy a link from a different section of their candidate profile.** For pending offer approvals needed from the CPO/CEO, there is an `#offers` Slack channel where the requests should be added. This is especially relevant if the CPO is out of office and the CEO is approving offers; the CEO should always be @mentioned for their approval. This Slack channel is private and only the recruiting team, CPO, CEO, and CFO have access to it.

### Communicating the Offer

Once the offer package has been approved by the approval chain, the verbal offer will be given, which will be followed by an official contract, which is sent through Greenhouse.

Offers made to new team members should be documented in Greenhouse through the notes thread between the person authorized to make the offer and the Candidate Experience Specialist.
   -  Greenhouse offer details should be updated as necessary to reflect any changes including start date. Sections updated that will trigger re-approval are noted in Greenhouse.

### Next Steps

One person from the recruiting team (typically the [Candidate Experience Specialists](https://about.gitlab.com/job-families/people-ops/candidate-experience-specialist/)) will prepare the contract:

   1. Check all aspects of the offer:
      - Do we have the new team members' legal name in their profile?
      - Is the new team members' address listed on the details page?
      - What contract type and entity are required based upon location and offer details?
      - Is it clear how many (if any) stock options this person should receive?
      - Is all necessary information (start date, salary, location, etc.) up to date?
      - Does the new team member need a work permit or visa, or require an update to them before a start date can be agreed?
   1. [Generate the contract within Greenhouse](https://about.gitlab.com/handbook/contracts/#how-to-use) using a template based on the details found in the offer package.
   1. Contact the recruiter or new team member to gather any missing pieces of information (note: the address can be found on the background check information page).
   1. Ensure that, if the contract was created outside of Greenhouse, the contract has been reviewed and approved by the Senior Director of Legal Affairs or a People Ops team member in the [People Ops confidential Slack channel](peopleops-confidentia).
   1. [Stage the contract in DocuSign from within Greenhouse](https://about.gitlab.com/handbook/contracts/#how-to-use), which emails the contract to the signing parties, with the recruiter, recruiting manager, and the hiring manager cc'd. The Director of Recruiting will sign all contracts before they go to the candidate to sign. If the Director of Recruiting is out of office, the CPO will sign.
   1. Enter the new team member's details on the Google sheet [GitLab Onboarding Tracker](https://docs.google.com/spreadsheets/d/1L1VFODUpfU249E6OWc7Bumg8ko3NXUDDeCPeNxpE6iE/edit?usp=sharing) and continually update it during the hiring process.
   1. When the contract is signed by all parties, the Candidate Experience Specialist verifies that the start date in Greenhouse is correct; then they will mark the candidate in Greenhouse as "Hired." Ensure that the "Hired" date in Greenhouse matches the date the contract was signed, and thanks to an integration between Greenhouse and BambooHR, it will automatically add an entry for the new team member in BambooHR (do not export to BambooHR if it is an internal hire).
   1. The Candidate Experience Specialist will upload the signed contract and the completed background check into the BambooHR profile.
   1. The Candidate Experience Specialist will email the new team member the Welcome Email from Greenhouse with a Cc to the People Ops email alias, recruiter, recruiting manager and hiring manager.
      * The notebook order form is included in this email. IT Ops will order the applicable notebook when this form is completed by the new team member.
   1. The recruiter will unpublish the vacancy in Greenhouse and disposition any remaining candidates if necessary. Once complete, the recruiter will ping the Candidate Experience Specialist to close the role or close the role themselves.
   1. The Candidate Experience Specialist will create an onboarding issue shell (https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md)
      1. Create the title: Onboarding (new team member's first name and last name), starting (ISO Date) as (job title).
      2. Type x between the brackets for the first task.
      3. Assign the hiring manger in the second task with their GitLab handle.
      4. In the "Day 1" section, delete any information that is not applicable to the new team member based upon their location.
      5. Make it confidential by checking the confidential box at the very bottom.
      6. Assign it to the hiring manager and all People Operations Specialists.
      7. Submit the issue.
      8. Comment on the issue to let People Ops know whether or not they should close the vacancy.
      9. Copy and paste the link into the Google sheet [GitLab Onboarding Tracker](https://docs.google.com/spreadsheets/d/1L1VFODUpfU249E6OWc7Bumg8ko3NXUDDeCPeNxpE6iE/edit?usp=sharing).

People Operations will start the [onboarding tasks](https://about.gitlab.com/handbook/general-onboarding/onboarding-processes/) no later than one week before the new team member joins.

For questions about the new team member's onboarding status, view the People Operations Specialists assigned via the Google sheet [GitLab Onboarding Tracker](https://docs.google.com/spreadsheets/d/1L1VFODUpfU249E6OWc7Bumg8ko3NXUDDeCPeNxpE6iE/edit?usp=sharing) and @mention them in the [People Ops confidential Slack channel](#peopleops-confidentia).

For questions about the new team member's laptop, ping [IT Ops](#it-ops) in Slack. If the questions arise through email, forward the email to itops@gitlab.com and ping IT Ops in #it-ops Slack, and @it-ops-team too due to volume.
