---
layout: markdown_page
title: "Professional Service Engineering"
---
# Professional Service Engineering Handbook
{:.no_toc}

The Professional Service Engineering group at GitLab is a part of the [Customer Success](/handbook/customer-success) department. Professional Service Engineers have the goal to optimize organization's adoption of GitLab through our professional services, designed to enable other necessary systems in your environment so that customers can move from planning to monitoring.

## On this page
{:.no_toc}

- TOC
{:toc}

## Role & Responsibilities
See the [Professional Service Engineer role description](/job-families/sales/professional-service-engineer/)

### Statement of Work creation

The GitLab Professional Service Engineering group is responsible for both maintaining the [GitLab SOW Template](https://docs.google.com/document/d/1X8_EiX8kgJdpaVlydbTJg5pn4RXeDvYOIyok2G1A69I/edit) (internal link) as well as the production of new Statements of Work for customer proposals.

To obtain a Statement of Work, open a new SOW issue using the template on the [Professional Services project](https://gitlab.com/gitlab-com/customer-success/professional-services/issues/new?issuable_template=SOW%20Request) (internal link). 
We prefer customers to mark up our agreement and SOW template if they request changes. If they require the use of their services terms or SOW, please contact the Professional Service Engineering group.

### Long Term Profitability Targets
The long term gross margin target for services is 35%.

### Services Delivery

#### Implementation Plan

Each services engagement will include an Implementation Plan compiled based on the Statement of Work and other discussions the Professional Service Engineering group has with the customer.  The Professional Service Engineering group also maintains the SOW template located [at this internal link](https://docs.google.com/document/d/1ohZtqR1mMZYWAOD7PBVc1xZIMGpzHNgccRxVZ9Nzirg/edit).

Each services engagement will include an Implementation Plan compiled based on the Statement of Work and other discussions the Professional Service Engineering group has with the customer. The Professional Service Engineering group also maintains the SOW template located [at this internal link](https://docs.google.com/document/d/1ohZtqR1mMZYWAOD7PBVc1xZIMGpzHNgccRxVZ9Nzirg/edit).

Each services engagement will have a google sheet that shows the contracted revenue and the estimated cost measured in hours or days of effort required to complete the engagement.  The cost estimate must be completed prior to SoW signature and attached to the opportunity in SFDC.


### Professional Service Engineering Workflows

For details on the specific Professional Service Engineering plays, see [professional service engineering workflows](/handbook/customer-success/professional-service-engineering/workflows).

## Selling Professional Services

For details on selling professional services, see [Selling Professional Services](/handbook/customer-success/professional-service-engineering/selling).

## Professional Services Offerings

See [Professional Services Offerings](/handbook/customer-success/professional-service-engineering/offerings).

## How to work with/in the Professional Service Engineering group

### Contacting & Scheduling

At GitLab, Professional Service Engineering is part of the [Customer Success department](/handbook/customer-success).  As such, you can engage with Professional Service Engineering by following the guidelines for [engaging with any Solutions Architect](/handbook/customer-success/solutions-architects#when-and-how-to-engage-a-solutions-architect).  This process ensures that the Customer Success department as a whole can understand the inbound needs of the account executive and our customers.

For scheduling specific customer engagements, we currently are slotting implementations while our group grows to support the demand for services.  If you have a concern about scheduling the engagement, this should be discussed at the Discovery phase.  In no case should you commit to dates before receipt of agreements, P.O., etc.

### Contacting Professional Service Engineering

To contact the Professional Service Engineering group, the best way is to follow the guidelines for [Engaging a Solutions Architect](/handbook/customer-success/solutions-architects#when-and-how-to-engage-a-solutions-architect).

You can also reach the group via the [#customer-success Slack Channel](https://gitlab.slack.com/messages/C5D346V08/).

### Professional Service Engineering Issue Board

The [Professional Service Engineering Issue Board is available here](https://gitlab.com/gitlab-com/customer-success/professional-services-group/ps-plan/boards).  This board contains everything that the group is working on - from strategic initiatives to [SOW writing](#statement-of-work-creation), all group activity is available here.

#### Time Tracking

The professional services team will track time against each Statement of Work.

#### Project Completion

At the conclusion of the Statement of Work the Professional Service Engineer will prepare a cost vs actual analysis. This analysis will be filed in SFDC.  When filed in SFDC the Professional Service Engineer will @mention the the Controller and Finance Business Partner, Sales in SFDC Chatter.

#### SOW Issues

When requesting a SOW, Account Executives or Professional Service Engineering group members should use the SOW issue template, which automatically shows the required information as well as labels the issue appropriately.

#### Strategic Initiatives
Strategic Initiatives are undertaken by the Professional Service Engineering group to leverage team member time in a given area of the Customer Success Department.  This can include increased documentation, better training resources or program development.

