---
layout: markdown_page
title: "Category Vision - Maven Repository"
---

- TOC
{:toc}

## Maven Repository

GitLab's integration with Maven has provided a standardized way to share and version Java dependencies across projects. In the coming months, we will be adding templates for auto-deploying Maven packages to the GitLab registry and enabling publishing of Maven packages at the group level. 

As we evaluate additional package manager integrations, we will look to standardize our API, user permissions and the user interface. The Maven Repository and your feedback will help us to prioritize and set those standards.  

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Maven%20Repository)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1291) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

Next we will be providing users with a template for packages to the GitLab Maven Repository. [#ee-10051](https://gitlab.com/gitlab-org/gitlab-ee/issues/10051) Delivering this issue will make it easier to adopt the Maven repository, increasing user adoption and usability of the feature. 

## Maturity Plan
This category is currently at the "Minimal" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).
Key deliverables to achieve this are:

- [Support additional authentication methods (deploy tokens)](https://gitlab.com/gitlab-org/gitlab-ee/issues/10749)

## Competitive Landscape

- [JFrog Artifactory](https://www.jfrog.com/confluence/display/RTF/Maven+Repository)
- [Sonatype Nexus](https://www.sonatype.com/nexus-repository-sonatype)
- [GitHub](https://help.github.com/en/articles/configuring-apache-maven-for-use-with-github-package-registry) has a product in beta, which allows you to authenticate, publish and delete packages. In addition, they support webhooks for receiving registry events when a package is published or updated. 

Historically, we've provided access to the GitLab Container Registry for free, but limited access to additional integrations, such as Maven, to our paid tier. However, with [GitHub's annoucement](https://github.blog/2019-05-10-introducing-github-package-registry/) to support a package registry for free, we are reevaluating this strategy. 

## Top Customer Success/Sales Issue(s)

The top customer success and sales issues are related to improving and expanding the UI. [#ee-7932](https://gitlab.com/gitlab-org/gitlab-ee/issues/7932) seeks to improve the navigation for the entire GitLab registry and standardize our naming conventions. 

## Top Customer Issue(s)

Our top customer issues are related to simplifying our API protocols and improving how we handle permissions. [gitlab-ee#8280](https://gitlab.com/gitlab-org/gitlab-ee/issues/8280) focuses on making our API more user-friendly by eliminating the need to translate project_name to project_id. [gitlab-ee#10749](https://gitlab.com/gitlab-org/gitlab-ee/issues/10749#) seeks to expand authentication to include deploy tokens.


## Top Internal Customer Issue(s)

The top internal customer issue is tied to storage optimization. The first step is to understand the current and historical usage of the registry. [gitlab-ce#49412](https://gitlab.com/gitlab-org/gitlab-ce/issues/49412) will help us to measure historical usage and prioritize future improvements. 

## Top Vision Item(s)

The top vision item is [gitlab-ee#9095](https://gitlab.com/gitlab-org/gitlab-ee/issues/9095), which will add support for the dependency proxy to the Maven repository. Once launched, it will add security scanning and vulnerability testing.