---
layout: markdown_page
title: "Category Vision - Web IDE"
---

- TOC
{:toc}

## Web IDE

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas, 
and user journeys into this section. -->

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=web%20ide)
- [Epic List](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=web%20ide)
- [Overall Vision](/direction/create/)

Please reach out to PM Kai Armstrong ([E-Mail](mailto:karmstrong@gitlab.com)) if you'd like to provide feedback or ask
questions about what's coming.

## Target Audience and Experience
<!-- An overview of the personas involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

## What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

**In progress (ETA 12.0): [Server side evaluation](https://gitlab.com/groups/gitlab-org/-/epics/167)** The Web IDE is currently a well featured web editor built on Monaco that is integrated with merge requests and CI, but it isn't yet a fully functional integrated development environment. Using GitLab CI runners we will open a Web Terminal to a CI runner when the Web IDE is opened so that tests can be run in real-time. Changes will be mirrored from the Web IDE to the CI runner. Once changes are mirrored live from the Web IDE to the runner, we should be able to expose a port to the runner and serve contents from the development environment.

## Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

- [Eclipse Che](https://www.eclipse.org/che/)
- [Cloud9](https://aws.amazon.com/cloud9/)
- [Codesandbox](https://codesandbox.io/)
- [Repl.it](https://repl.it/)
- [Koding](https://www.koding.com/)

## Market Research
<!-- This section should link or highlight any relevant market research you've done that justifies our
entry into the market for the particular category. -->

## Business Opportunity
<!-- This section should highlight the business opportunity highlighted by the particular category. -->

## Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

Web IDE's don't appear to be a particular area of analysis currently.

- https://www.g2crowd.com/categories/integrated-development-environment-ide
- https://www.theserverside.com/news/450433105/AWS-Cloud9-IDE-threatens-Microsoft-developer-base

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

- Linting in the Web IDE https://gitlab.com/groups/gitlab-org/-/epics/70
